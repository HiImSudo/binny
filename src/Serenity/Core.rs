use std::env;
use std::fs::File;
use std::io::prelude::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MemoryBlock{
    pub data: Vec<u8>,
    pub base_address: Option<u64>,
}

impl<'a> MemoryBlock{
    pub fn from_file(filename: &str, base_address: Option<u64>) -> Result<MemoryBlock, ()>{
         let mut f = File::open(filename).expect("File Not Found");
         let mut buffer: Vec<u8> = Vec::new();
         f.read_to_end(&mut buffer).expect("Could Not Read Entire File");

        Ok(MemoryBlock::new(buffer, base_address))
    }

    pub fn to_file(&self, filename: &str) -> Result<(),()>{
        let mut f = File::create(filename).expect("Could Not Create File");
        f.write_all(&self.data).expect("Could Not Write Entire Buffer");
        
        Ok(())
    }

    pub fn new(data: Vec<u8>, base_address: Option<u64>) -> MemoryBlock{
        MemoryBlock{
            data,
            base_address
        }
    }

    pub fn eval_block(&self, another: &'a MemoryBlock) -> BlockChange{
        if another.data.len() > self.data.len(){
            BlockChange::new(_BlockChange::GROW, another.data.len() - self.data.len())
        }else if another.data.len() < self.data.len(){
            BlockChange::new(_BlockChange::SHRINK, self.data.len() - another.data.len())            
        }else{
            BlockChange::new(_BlockChange::NONE, 0)                        
        }
    }

    pub fn eval_bytes(&self, another: &'a MemoryBlock) -> MemoryChange{
        let block_change = self.eval_block(another);
        let mut byte_changes: Vec<ByteChange> = Vec::new();

        match block_change.change_type{
            _BlockChange::GROW =>{
                for i in 0..self.data.len(){
                    if self.data[i] != another.data[i]{
                        byte_changes.push(ByteChange::new(i as u64, Some(self.data[i]), Some(another.data[i])));
                    }
                }

                for i in self.data.len()..another.data.len(){
                        byte_changes.push(ByteChange::new(i as u64, None, Some(another.data[i])));
                }
            }
            _BlockChange::SHRINK =>{
                for i in 0..another.data.len(){
                    if self.data[i] != another.data[i]{
                        byte_changes.push(ByteChange::new(i as u64, Some(self.data[i]), Some(another.data[i])));
                    }
                }

                for i in another.data.len()..self.data.len(){
                        byte_changes.push(ByteChange::new(i as u64, Some(self.data[i]), None));
                }
            }
            _BlockChange::NONE =>{
                for i in 0..self.data.len(){
                    if self.data[i] != another.data[i]{
                        byte_changes.push(ByteChange::new(i as u64, Some(self.data[i]), Some(another.data[i])));
                    }
                }
            }
        }

        MemoryChange::new(block_change, byte_changes)
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct BlockChange{
    pub change_type: _BlockChange,
    pub size: usize,
}

impl BlockChange{
    pub fn new(change_type: _BlockChange, size: usize) -> BlockChange{
        BlockChange{
            change_type,
            size
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum _BlockChange{
    SHRINK = -1,
    NONE = 0,
    GROW = 1
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ByteChange{
    pub offset: u64,
    pub old_value: Option<u8>,
    pub new_value: Option<u8>,
}

impl ByteChange{
    fn new(offset: u64, old_value: Option<u8>, new_value: Option<u8>) -> ByteChange{
        ByteChange{
            offset,
            old_value,
            new_value
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MemoryChange{
    pub block_change: BlockChange,
    pub byte_changes: Vec<ByteChange>
}

impl MemoryChange{
    pub fn new(block_change: BlockChange, byte_changes: Vec<ByteChange>) -> MemoryChange{
        MemoryChange{
            block_change,
            byte_changes
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MemoryTimeline{
    pub base_block: MemoryBlock,
    pub current_block: MemoryBlock,
    pub changes: Vec<MemoryChange>
}

impl MemoryTimeline{
    pub fn new(base_block: MemoryBlock) -> MemoryTimeline{
        MemoryTimeline{
            base_block: base_block.clone(),
            current_block: base_block.clone(),
            changes: Vec::new()
        }
    }

    pub fn eval(&mut self, block: &MemoryBlock){
        self.changes.push(self.current_block.eval_bytes(&block));
        self.current_block = block.clone();
    }
    
}