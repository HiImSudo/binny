use crate::{MemoryBlock, MemoryChange, _BlockChange};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MemoryBlockTransformer{} // Dummy struct

impl<'a> MemoryBlockTransformer{
    pub fn apply(base_block: &mut MemoryBlock, changes: Vec<MemoryChange>){
        let mut buffer: Vec<u8> = base_block.data.clone();
        for change in changes{
            match change.block_change.change_type{
                _BlockChange::GROW =>{
                    buffer.resize(base_block.data.len() + change.block_change.size, 0);
                },
                _BlockChange::SHRINK =>{
                    buffer.resize(base_block.data.len() - change.block_change.size, 0);
                },
                _BlockChange::NONE =>{

                }
            }
            for byte_change in change.byte_changes{
                if byte_change.offset < (buffer.len() as u64){
                    buffer[byte_change.offset as usize] = byte_change.new_value.unwrap();
                }
            }
        }

        // Replace base buffer
        base_block.data = buffer;
    }

    pub fn apply_single(base_block: &mut MemoryBlock, change: &MemoryChange){
        let mut buffer: Vec<u8> = base_block.data.clone();
        match change.block_change.change_type{
            _BlockChange::GROW =>{
                buffer.resize(base_block.data.len() + change.block_change.size, 0);
            },
            _BlockChange::SHRINK =>{
                buffer.resize(base_block.data.len() - change.block_change.size, 0);
            },
            _BlockChange::NONE =>{
            
            }
        }
        
        for byte_change in &change.byte_changes{
            if byte_change.offset < (buffer.len() as u64){
                buffer[byte_change.offset as usize] = byte_change.new_value.unwrap();
            }
        }

        // Replace base buffer
        base_block.data = buffer;
    }

    pub fn apply_till(base_block: &mut MemoryBlock, changes: Vec<MemoryChange>, end_at: u64){
        if end_at > changes.len() as u64{
            panic!();
        }

        for i in 0..end_at{
            MemoryBlockTransformer::apply_single(base_block, &changes[i as usize]);
        }
    }

    pub fn generate_new(base_block: &MemoryBlock, changes: Vec<MemoryChange>) -> MemoryBlock{
        let mut new_block: MemoryBlock = base_block.clone();

        MemoryBlockTransformer::apply(&mut new_block, changes);
        new_block
    }

    pub fn generate_new_single(base_block: &MemoryBlock, change: MemoryChange) -> MemoryBlock{
        let mut new_block: MemoryBlock = base_block.clone();

        MemoryBlockTransformer::apply_single(&mut new_block, &change);
        new_block
    }

    pub fn generate_new_till(base_block: &MemoryBlock, changes: Vec<&MemoryChange>, end_at: u64) -> MemoryBlock{
        let mut new_block: MemoryBlock = base_block.clone();

        if end_at > changes.len() as u64{
            panic!();
        }

        for i in 0..end_at{
            MemoryBlockTransformer::apply_single(&mut new_block, &changes[i as usize]);
        }
        new_block
    }
}
