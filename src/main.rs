#[macro_use]
extern crate serde_derive;

use  std::net::{TcpListener, TcpStream};
use std::io;
use std::io::prelude::*;
use std::fs::File;

extern crate serde_json;
use serde_json::{Value, Error};

extern crate base64;
use base64::{encode, decode};

mod Serenity;
use crate::Serenity::Core::*;
use crate::Serenity::Transformers::{MemoryBlockTransformer};

use std::collections::HashMap;

extern crate signal_hook;

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};


fn main() {
    let term = Arc::new(AtomicBool::new(false));
    let mut server = TcpListener::bind("0.0.0.0:9685").expect("Could not bind socket");
    let mut block_timelines: HashMap<u64,MemoryTimeline> = HashMap::new();
    loop{
        let mut stream: TcpStream = server.accept().unwrap().0;
        let mut buffer: Vec<u8> = Vec::new();
        match stream.read_to_end(&mut buffer){
            Ok(n) =>{
                let x: Result<Value, _> = serde_json::from_slice(buffer.as_slice());
                match x{
                    Ok(json) =>{
                        let data = decode(&str::replace(&json["data"].to_string(), "\"", "")).expect("Could not decode base64");
                        let base_address = json["base_address"].as_u64().unwrap_or(0);
                        let block = MemoryBlock::new(data, Some(base_address));
                        let instruction = json["instruction"].as_str().unwrap_or("none");

                        match block_timelines.get_mut(&base_address){
                            Some(timeline) =>{
                                println!("UPDATING BLOCK 0x{:x}", base_address);
                                timeline.eval(&block);
                            },
                            None=>{
                                println!("NEW BLOCK 0x{:x}", base_address);
                                block_timelines.insert(base_address, MemoryTimeline::new(block));
                            }
                        }

                        if instruction == "stop"{
                            println!("Stopping");
                            serde_json::to_writer(&File::create("data.json").expect("Error creating output json."), &block_timelines).expect("Error Binding To Writer");
                            break;
                        }
                    }
                    Err(_) =>{
                        println!("Couldn't parse json.");
                    }
                }
            }
            Err(e) =>{
                println!("Error: {:?}", e);
            }
        }
        drop(stream);
    }
}
